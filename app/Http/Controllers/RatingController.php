<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRate;
use App\Repositories\RateRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;

class RatingController extends Controller
{
    protected $rate;

    public function __construct(RateRepository $rate)
    {
        $this->rate = $rate;
    }

    /**
     * @param \App\Http\Requests\StoreRate $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * StoreRate validate all request before calling method so data here is valid!
     */
    public function registerRate(StoreRate $request)
    {
        $res = $this->rate->addRate($request->all());

        $message = [
            'msg' => 'Rate Registered Successfully',
            'meeting' => $res
        ];
        return response()->json($message, 201);
    }

    /**
     * @param $rater_id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Illuminate\Foundation\Testing\HttpException
     * get list of rates that a raters registered
     */
    public function getRaterHistory($rater_id)
    {

        //use findBy method from RateRepository
        $res = $this->rate->findBy('rater_id', $rater_id);

        if ($res->isEmpty()) {
            throw new HttpException(404, 'rater Not Found!');
        }

        $message = [
            'msg' => 'List Of rater',
            'rates' => $res
        ];

        $status = 200;

        return response()->json($message, $status);

    }

    /**
     * @param $ratable_id
     *
     * @return \Illuminate\Http\JsonResponse
     * get all rates records tha a ratable earned
     */
    public function getRatableHistory($ratable_id)
    {
        //use findBy method from RateRepository
        $res = $this->rate->findBy('ratable_id', $ratable_id);
        if ($res->isEmpty()) {
            throw new HttpException(404, 'ratable Not Found!');
        }

        $message = [
            'msg' => 'List Of ratable',
            'rates' => $res
        ];

        $status = 200;

        return response()->json($message, $status);

    }


    /**
     * @param $ratable_id
     *
     * @param $ratable_type
     *
     * @return \Illuminate\Http\JsonResponse get average of rates earned by a ratable
     * get average of rates earned by a ratable
     */
    public function getAverage($ratable_type, $ratable_id)
    {
        $att = ['ratable_id' => $ratable_id, 'ratable_type' => $ratable_type];
        $res = $this->rate->calculateAvg($att, 'rate');

        if (empty($res)) {
            throw new HttpException(404, 'Not Found!');
        }

        $message = [
            'msg' => 'Average Of Rates',
            'Average' => $res
        ];

        $status = 200;

        return response()->json($message, $status);

    }

}
