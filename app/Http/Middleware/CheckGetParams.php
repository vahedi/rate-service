<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CheckGetParams
{

    /**
     * @param          $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        foreach ($request->route()->parameters() as $item){

            if (!is_numeric($item)) {
                throw new HttpException(400, 'invalid get Parameter!');
            }
        }
        return $next($request);
    }
}
