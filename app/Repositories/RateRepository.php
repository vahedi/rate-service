<?php

namespace App\Repositories;

use App\Rating;

class RateRepository
{

    protected $Rating;

    public function __construct(Rating $Rating)
    {
        $this->rating = $Rating;
    }

    public function findBy($att, $column)
    {

        return $this->rating->where($att, $column)->get();

    }

    public function calculateAvg($att, $field)
    {

        return $this->rating->where($att)->get()->Avg($field);

    }

    public function addRate($att)
    {

        return Rating::create($att);

    }
}

?>