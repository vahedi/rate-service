<?php

Route::prefix('/v0/rate')->group(function () {

    Route::post('/', 'RatingController@registerRate');

    Route::group(['middleware' => 'checkParams'], function () {
        Route::get('/{rater_id}', 'RatingController@getRaterHistory');
        Route::get('/ratable/{ratable_id}', 'RatingController@getRatableHistory');
        Route::get('/average/{ratable_type}/{ratable_id}', 'RatingController@getAverage');
    });
});

